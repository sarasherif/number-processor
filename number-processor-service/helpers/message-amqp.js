const amqp = require('amqplib');

const RABBITMQ_URL = process.env.RABBITMQ_URL || 'amqp://@localhost:5672';

let channel = null;

const createMqChannel = async () => {
  if (channel) {
    return channel;
  }
  const connection = await amqp.connect(RABBITMQ_URL);
  channel = await connection.createChannel();
  return channel;
};

(async () => {
  const ch = await createMqChannel();
  if (ch) console.log(`connected to messageQueue on ${RABBITMQ_URL} and channel created`);
}
)();


module.exports = {
  async consume(queue, cb) {
    const channel = await createMqChannel();
    await channel.assertQueue(queue, { durable: false});
    await channel.prefetch(1);
    channel.consume(queue, async (msg) => {
      if (msg) {
        const msgContent = JSON.parse(msg.content.toString());
        await cb(msgContent);
        channel.ack(msg);
      }
    }, { noAck: false });
  },
};