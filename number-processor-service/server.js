const express = require('express');

const app = express();
const server = require('http').createServer(app);
const redis = require('redis');
const amqpModule = require('./helpers/message-amqp');
const CronJob = require('cron').CronJob;
const port = process.env.PORT || 4001;
const REDIS_URL = process.env.REDIS_URL || 'redis://localhost:6379';
const ID = process.env.ID || `— ID_${Math.floor(Math.random() * 10)} —`;


const redisClient = redis.createClient(REDIS_URL);

redisClient.on('connect', function() {
  console.log('Redis now connected');
});

new CronJob({
  cronTime: '*/3 * * * * *',
  async onTick() {
    // get number from generated service
    amqpModule.consume('numberGenerated', async (msg) => {
      redisClient.set(ID, msg);
    });
  },
  runOnInit: true,
  start: true,
});


app.get('/', (req, res) => {
  res.send('Number processor service');
});

app.get('/numbers', (req,res) => {     
  redisClient.get('*', function(error, reply) {               
    if(error) return res.status(500).json({error: error});  
    return res.status(200).json({ data: `${ID} ${reply}` });                           
  });
});

server.listen(port, () => {
  console.log('Server running on port', port);
});

