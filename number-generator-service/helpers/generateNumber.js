let start = -1;

const generateNumber = (end)=>{
  start++;
  return  start + 1 < end  ? start + 1 : end;
};

module.exports = generateNumber;

