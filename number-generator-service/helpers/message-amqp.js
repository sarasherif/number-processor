const amqp = require('amqplib');

const RABBITMQ_URL = process.env.RABBITMQ_URL || 'amqp://@localhost:5672';


let channel = null;

const createMqChannel = async () => {
  if (channel) {
    return channel;
  }
  const connection = await amqp.connect(RABBITMQ_URL);
  channel = await connection.createChannel();
  return channel;
};

(async () => {
  const ch = await createMqChannel();
  if (ch) console.log(`connected to messageQueue on ${RABBITMQ_URL} and channel created`);
}
)();


module.exports = {
  async publish(queue, msg) {
    const channel = await createMqChannel();
    channel.sendToQueue(queue, Buffer.from(JSON.stringify(msg)), {
      persistent: true,
    });
  },
};