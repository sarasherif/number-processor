const express = require('express');

const app = express();
const server = require('http').createServer(app);
const generateNumber = require('./helpers/generateNumber');
const CronJob = require('cron').CronJob;
const amqpModule = require('./helpers/message-amqp');
const port = process.env.PORT || 4000;

new CronJob({
    cronTime: '*/2 * * * * *',
    async onTick() {
      const number = generateNumber(10000);
      console.log('This message every 2 sec from generator service');
      amqpModule.publish('numberGenerated', number);
      if(number === 10000 || number > 10000) this.stop()
    },
    runOnInit: true,
    start: true,
});


app.get('/', (req, res) => {
  res.send('Number generator service');
});

// listen of server
server.listen(port, () => {
  console.log('Server running on port', port);
});

